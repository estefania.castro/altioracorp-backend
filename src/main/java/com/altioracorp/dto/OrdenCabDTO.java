package com.altioracorp.dto;

import java.util.ArrayList;
import java.util.List;

import com.altioracorp.model.OrdenCab;
import com.altioracorp.model.OrdenDet;
import com.altioracorp.util.DateUtil;

public class OrdenCabDTO {
	
	private Long id;
	private String fecha;
	private ClienteDTO cliente;
	private List<OrdenDetDTO> ordenDetList;
	
	public OrdenCabDTO() {		
	}

	public OrdenCabDTO(OrdenCab entity) {
		this.id = entity.getId();
		this.fecha = DateUtil.format_yyyy_MM_dd(entity.getFecha());
		this.cliente = new ClienteDTO(entity.getCliente());
		this.ordenDetList = buildDetailList(entity.getOrdenDetList());
	}
	
	public List<OrdenDetDTO> buildDetailList(List<OrdenDet> detailList){
		List<OrdenDetDTO> dtoDetailList = new ArrayList<OrdenDetDTO>();
		
		detailList.forEach(detail -> {
			dtoDetailList.add(new OrdenDetDTO(detail));
		});
		
		return dtoDetailList;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public List<OrdenDetDTO> getOrdenDetList() {
		return ordenDetList;
	}

	public void setOrdenDetList(List<OrdenDetDTO> ordenDetList) {
		this.ordenDetList = ordenDetList;
	}

}
