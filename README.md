# Altioracorp-BackEnd

### Instrucciones para arrancar el proyecto

- Se debe cambiar los datos de conexion de base de datos ubicados en el archivo application.properties
- Dentro de la carpeta principal del proyecto ejecutar 'mvn clean install'
- Luego ejecutar 'java -jar target\order-0.0.1-SNAPSHOT.jar'
- La aplicación se encuentra corriendo en 'localhost:8080'