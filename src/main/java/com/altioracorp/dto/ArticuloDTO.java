package com.altioracorp.dto;

import java.math.BigDecimal;

import com.altioracorp.model.Articulo;

public class ArticuloDTO {
	
	private Long id;	
	private String codigo;	
	private String nombre;	
	private BigDecimal precioUnitario;	
	private Boolean activo;
	private Integer stock;
	
	public ArticuloDTO() {
	}
	
	public ArticuloDTO(Articulo entity) {
		this.id = entity.getId();
		this.codigo = entity.getCodigo();
		this.nombre = entity.getNombre();
		this.precioUnitario = entity.getPrecioUnitario();
		this.activo = entity.getActivo();
		this.stock = entity.getStock();
	}
	
	public Articulo toEntity() {
		Articulo entity = new Articulo();
		entity.setId(id);
		entity.setCodigo(codigo);
		entity.setNombre(nombre);
		entity.setPrecioUnitario(precioUnitario);
		entity.setActivo(activo);
		entity.setStock(stock);
		return entity;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}	
	
}
