package com.altioracorp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altioracorp.dto.ClienteDTO;
import com.altioracorp.model.Cliente;
import com.altioracorp.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository repository;
	
	public ClienteDTO create(ClienteDTO dto) {
		Cliente entity = dto.toEntity();
		repository.save(entity);
		return new ClienteDTO(entity);
	}
	
	public List<ClienteDTO> findAll(){
		List<Cliente> entities = repository.findAll();
		return buildDtoList(entities);
	}
	
	private List<ClienteDTO> buildDtoList(List<Cliente> entities){
		List<ClienteDTO> dtoList = new ArrayList<ClienteDTO>();
		entities.forEach(cliente ->{
			dtoList.add(new ClienteDTO(cliente));
		});
		
		return dtoList;
	}

}
