package com.altioracorp.dto;

import com.altioracorp.model.Cliente;

public class ClienteDTO {
	
	private Long id;
	private String nombre;
	private String apellido;
	
	public ClienteDTO() {
		
	}
	
	public ClienteDTO(Cliente entity) {
		this.id = entity.getId();
		this.nombre = entity.getNombre();
		this.apellido = entity.getApellido();
	}
	
	public Cliente toEntity() {
		Cliente entity = new Cliente();
		entity.setId(id);
		entity.setNombre(nombre);
		entity.setApellido(apellido);
		return entity;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
}
