package com.altioracorp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.altioracorp.dto.ArticuloDTO;
import com.altioracorp.exception.NotCreatedException;
import com.altioracorp.service.ArticuloService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/articulo")
@CrossOrigin(origins = "*")
public class ArticuloController {
	
	@Autowired
	private ArticuloService service;
	
	@GetMapping(value = "/activos")
	@ResponseBody
	public List<ArticuloDTO> findActives(){
		return service.findActives();
	}
	
	@GetMapping
	@ResponseBody
	public List<ArticuloDTO> findAll(){
		return service.findAll();
	}
	
	@GetMapping(value = "/codigo/{codigo}")
	@ResponseBody
	public ArticuloDTO findByCodigo(@PathVariable("codigo") String codigo) {
		try {
			return service.findArticuloByCodigo(codigo);
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
	
	@PostMapping
	@ResponseBody
	public ArticuloDTO create(@RequestBody ArticuloDTO dto) {
		return service.create(dto);
	}
	
	@PutMapping
	@ResponseBody
	public ArticuloDTO update(@RequestBody ArticuloDTO dto) {
		try {
			return service.update(dto);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
	
	@DeleteMapping(value = "/{id}")
	@ResponseBody
	public void delete(@PathVariable("id") Long id) {
		try {
			service.delete(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
	
	@GetMapping(value = "/stock/{id}/{cantidad}")
	@ResponseBody
	public void findStock(@PathVariable("id") Long id, @PathVariable("cantidad") Integer cantidad) {
		try {
			service.findStock(id,cantidad);
		} catch (NotCreatedException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Stock incompleto");
		} 
	}

}
