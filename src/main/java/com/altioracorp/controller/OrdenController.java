package com.altioracorp.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.altioracorp.dto.OrdenCabDTO;
import com.altioracorp.exception.NotCreatedException;
import com.altioracorp.service.OrdenCabService;

@RestController
@RequestMapping(value = "/orden")
@CrossOrigin(origins = "*")
public class OrdenController {

	@Autowired
	private OrdenCabService service;
	
	@PostMapping
	@ResponseBody
	public OrdenCabDTO create(@RequestBody OrdenCabDTO dto) {
		try {
			return service.create(dto);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en formato de fecha");
		} catch (NotCreatedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Stock incompleto");
		} 
	}
	
	@GetMapping
	@ResponseBody
	public List<OrdenCabDTO> findAll(){
		return service.findAll();
	}
	
}
