package com.altioracorp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.altioracorp.dto.ClienteDTO;
import com.altioracorp.service.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
@CrossOrigin(origins = "*")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@PostMapping
	@ResponseBody
	public ClienteDTO create(@RequestBody ClienteDTO dto) {
		return service.create(dto);
	}
	
	@GetMapping
	@ResponseBody
	public List<ClienteDTO> findAll(){
		return service.findAll();
	}

}
