package com.altioracorp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static Date toDate_yyyy_MM_dd(String date) throws ParseException {
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(date);
	}
	
	public static String format_yyyy_MM_dd(Date date) {
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		String text = sdf.format(date);
		return text;
	}
}
