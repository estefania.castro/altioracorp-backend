package com.altioracorp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.altioracorp.model.OrdenCab;

@Repository
public interface OrdenCabRepository extends JpaRepository<OrdenCab, Long> {

}
