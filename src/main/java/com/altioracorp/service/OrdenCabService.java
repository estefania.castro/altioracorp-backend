package com.altioracorp.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altioracorp.dto.OrdenCabDTO;
import com.altioracorp.dto.OrdenDetDTO;
import com.altioracorp.exception.NotCreatedException;
import com.altioracorp.model.Articulo;
import com.altioracorp.model.Cliente;
import com.altioracorp.model.OrdenCab;
import com.altioracorp.model.OrdenDet;
import com.altioracorp.repository.ArticuloRepository;
import com.altioracorp.repository.OrdenCabRepository;
import com.altioracorp.util.DateUtil;


@Service
public class OrdenCabService {

	@Autowired
	private OrdenCabRepository repository;
	@Autowired
	private ArticuloRepository articuloRepository;
	
	public OrdenCabDTO create(OrdenCabDTO dto) throws ParseException, NotCreatedException {
		OrdenCab entity = new OrdenCab();
		entity.setId(dto.getId());		
		entity.setFecha(DateUtil.toDate_yyyy_MM_dd(dto.getFecha()));
		entity.setCliente(new Cliente(dto.getCliente().getId()));
		entity.setOrdenDetList(buildEntityList(dto.getOrdenDetList(),entity));
		repository.save(entity);
		return new OrdenCabDTO(entity);
	}
	
	public List<OrdenCabDTO> findAll(){
		List<OrdenCab> entities = repository.findAll();
		return buildDtoList(entities);
	}
	
	private List<OrdenCabDTO> buildDtoList(List<OrdenCab> entities){
		List<OrdenCabDTO> dtoList = new ArrayList<OrdenCabDTO>();
		entities.forEach(orden ->{
			dtoList.add(new OrdenCabDTO(orden));
		});
		
		return dtoList;
	}
	
	
	public List<OrdenDet> buildEntityList(List<OrdenDetDTO> detailList, OrdenCab cab) throws ParseException, NotCreatedException {
		List<OrdenDet> dtoDetailList = new ArrayList<OrdenDet>();
		
		for(OrdenDetDTO detail: detailList) {
			OrdenDet detEntity;
			detEntity = toEntityOrdenDet(detail);	
			Articulo a = detEntity.getArticulo();
			Integer stockActual = a.getStock() - detEntity.getCantidad();
			if(stockActual < 0 ) {
				throw new NotCreatedException("No hay stock para el articulo " + a.getNombre());
			}
			a.setStock(stockActual);
			detEntity.setCab(cab);				
			dtoDetailList.add(detEntity);
		}
		
		return dtoDetailList;
	}
	
	public OrdenDet toEntityOrdenDet(OrdenDetDTO dto) throws ParseException {
		OrdenDet entity = new OrdenDet();
		entity.setId(dto.getId());
		entity.setArticulo(articuloRepository.getOne(dto.getArticulo().getId()));
		entity.setPrecioUnitario(dto.getPrecioUnitario());
		entity.setCantidad(dto.getCantidad());
		return entity;
	}
}
