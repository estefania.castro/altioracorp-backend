package com.altioracorp.dto;

import java.math.BigDecimal;

import com.altioracorp.model.OrdenDet;

public class OrdenDetDTO {
	
	private Long id;
	private OrdenCabDTO cab;
	private ArticuloDTO articulo;
	private BigDecimal precioUnitario;
	private Integer cantidad;
	
	public OrdenDetDTO() {		
	}

	public OrdenDetDTO(OrdenDet entity) {
		this.id = entity.getId();
		this.articulo = new ArticuloDTO(entity.getArticulo());
		this.precioUnitario = entity.getPrecioUnitario();
		this.cantidad = entity.getCantidad();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrdenCabDTO getCab() {
		return cab;
	}

	public void setCab(OrdenCabDTO cab) {
		this.cab = cab;
	}

	public ArticuloDTO getArticulo() {
		return articulo;
	}

	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}

	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
}
