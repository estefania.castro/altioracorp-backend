package com.altioracorp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.altioracorp.model.OrdenDet;

public interface OrdenDetRepository extends JpaRepository<OrdenDet, Long> {
	
	@Query("SELECT o FROM OrdenDet o WHERE o.articulo.id = :id")
	public List<OrdenDet> findByArticulo(Long id);

}
