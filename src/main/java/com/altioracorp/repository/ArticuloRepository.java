package com.altioracorp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.altioracorp.model.Articulo;

@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Long> {
	
	@Query("SELECT a FROM Articulo a WHERE a.codigo = :codigo")
	public Articulo findArticuloByCodigo(@Param("codigo") String codigo);
	
	@Query("SELECT a FROM Articulo a WHERE a.activo = true")
	public List<Articulo> findActives();

}
