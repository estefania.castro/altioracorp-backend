package com.altioracorp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altioracorp.dto.ArticuloDTO;
import com.altioracorp.exception.NotCreatedException;
import com.altioracorp.model.Articulo;
import com.altioracorp.model.OrdenDet;
import com.altioracorp.repository.ArticuloRepository;
import com.altioracorp.repository.OrdenDetRepository;

import javassist.NotFoundException;

@Service
public class ArticuloService {
	
	@Autowired
	private ArticuloRepository repository;
	@Autowired
	private OrdenDetRepository detRepository;
	
	public ArticuloDTO findArticuloByCodigo(String codigo) throws NotFoundException {
		Articulo entity = repository.findArticuloByCodigo(codigo);
		if(entity == null) {
			throw new NotFoundException("No se encontro articulo");
		}
		return new ArticuloDTO(entity);
	}
	
	public ArticuloDTO create(ArticuloDTO dto) {
		Articulo entity = dto.toEntity();
		repository.save(entity);
		return new ArticuloDTO(entity);
	}
	
	public ArticuloDTO update(ArticuloDTO dto) throws NotFoundException {
		
		Optional<Articulo> entityOpt = repository.findById(dto.getId());
		if(entityOpt.isPresent()) {
			Articulo entity = entityOpt.get();
			entity = entity.fromDTO(dto);
			repository.save(entity);
			return new ArticuloDTO(entity);
		}else {
			throw new NotFoundException("No se encontró articulo");
		}
	}
	
	public void delete(Long id) throws NotFoundException {
		Optional<Articulo> entityOpt = repository.findById(id);
		if(entityOpt.isPresent()) {
			List<OrdenDet> ordenArticulo = detRepository.findByArticulo(id);
			if(ordenArticulo.size() > 1) {
				throw new NotFoundException("El articulo tiene una orden creada");
			}else {
				repository.delete(entityOpt.get());
			}
		}else {
			throw new NotFoundException("No se encontró articulo");
		}
	}
	
	public List<ArticuloDTO> findActives(){
		List<Articulo> entities = repository.findActives();
		return buildDtoList(entities);
	}
	
	public List<ArticuloDTO> findAll(){
		List<Articulo> entities = repository.findAll();
		return buildDtoList(entities);
	}
	
	private List<ArticuloDTO> buildDtoList(List<Articulo> entities){
		List<ArticuloDTO> dtoList = new ArrayList<ArticuloDTO>();
		entities.forEach(articulo ->{
			dtoList.add(new ArticuloDTO(articulo));
		});
		
		return dtoList;
	}
	
	public void findStock(Long id, Integer cantidad) throws NotCreatedException {
		
		Articulo a = repository.getOne(id);
		Integer stockActual = a.getStock() - cantidad;
		if(stockActual < 0 ) {
			throw new NotCreatedException("No hay stock para el articulo " + a.getNombre());
		}
		
	}

}
